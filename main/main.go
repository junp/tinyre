package main

import (
	"fmt"
	"junp/tinyre/tinyre"
	"log"
)

func main() {
	re, err := tinyre.Compile(`a(bb)+a`)
	if err != nil {
		log.Fatal(err)
	}
	ok := re.Match("abba")
	fmt.Println(ok)
}
