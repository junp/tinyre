package tinyre

import (
	"testing"
)

func TestRe2Post(t *testing.T) {
	testdata := []struct {
		input string
		want  string
	}{
		{`a`, `a`},
		{`a+b`, `a+b.`},
		{`a|b`, `ab|`},
		{`a|b|c|d`, `ab|c|d|`},
		{`(ab)c`, `ab.c.`},
		{`(ab)?c`, `ab.?c.`},
		{`a(bc)`, `abc..`},
		{`a*(b|c)+`, `a*bc|+.`},
		{`a*(b|c)*`, `a*bc|*.`},
		{`a(bb)+a`, `abb.+.a.`},
		{`ab|ab`, `ab.ab.|`},
		{`(acc|b)?c`, `ac.c.b|?c.`},
	}
	for _, test := range testdata {
		post, _ := re2post(test.input)
		if post != test.want {
			t.Errorf("For input: %s, expected %s, but get: %s", test.input, test.want, post)
		}
	}
}

func TestMatch(t *testing.T) {
	testdata := []struct {
		expr string
		str  string
		want bool
	}{
		// case match
		{`a`, "a", true},
		{`a+b`, "aaaaaaaab", true},
		{`a|b`, "b", true},
		{`a|b|c|d`, "c", true},
		{`(ab)c`, "abc", true},
		{`(ab)?c`, "abc", true},
		{`a(bc)`, "abc", true},
		{`a*(b|c)+`, "aaaaaaaac", true},
		{`a(bb)+a`, "abbbbbba", true},
		{`ab|cd`, "cd", true},
		{`(acc|b)?c`, "accc", true},
		{`(chloe|shikamaru|foo|bar)+@qq(com|cn)+`, "foo@qqcn", true},

		// case not match
		{`(chloe|shikamaru|foo|bar)+@qq(com|cn)+`, "@qqcn", false},
	}
	for _, test := range testdata {
		re, _ := Compile(test.expr)
		have := re.Match(test.str)
		if have != test.want {
			if test.want == true {
				t.Errorf("input %s should match regexp %s, but have result false",
					test.str, test.expr)
			} else {
				t.Errorf("input %s should not match regexp %s, but have result true",
					test.str, test.expr)
			}
		}
	}
}
