package tinyre

import (
	"errors"
)

// TODO: add support for some meta chars
// TODO: add dfa as the cache for the nfa

type Lexer struct {
	s string
}

type Regexp struct {
	start *State
}

func re2post(expr string) (string, error) {
	// stack and stack pointer
	var ss [100]rune
	var sp = -1

	var post []rune
	// we need to use prec to determine whether
	// there is a implicit concat
	var prec rune = 0
	for _, c := range expr {
		switch c {
		case '(':
			// push ( to ss
			if prec != '|' && prec != 0 {
				for sp >= 0 && ss[sp] != '(' {
					post = append(post, ss[sp])
					sp--
				}
				sp++
				ss[sp] = '.'
			}
			sp++
			ss[sp] = '('
		case ')':
			if sp < 0 {
				return "", errors.New("invalid regexp.")
			}
			r := ss[sp]
			sp--
			for r != '(' {
				post = append(post, r)
				if sp < 0 {
					return "", errors.New("invalid regexp.")
				}
				r = ss[sp]
				sp--
			}
		case '+':
			fallthrough
		case '*':
			fallthrough
		case '?':
			post = append(post, c)
		case '|':
			for sp >= 0 && ss[sp] != '(' {
				post = append(post, ss[sp])
				sp--
			}
			sp++
			ss[sp] = '|'
		// concat
		default:
			if prec != '(' && prec != '|' && prec != 0 {
				for sp >= 0 && ss[sp] == '.' {
					post = append(post, ss[sp])
					sp--
				}
				sp++
				ss[sp] = '.'
			}
			post = append(post, c)
		}
		prec = c
	}
	for sp >= 0 {
		post = append(post, ss[sp])
		sp--
	}
	return string(post), nil
}

// NFA state node
type State struct {
	c      int32
	out    *State
	out1   *State
	lastid int
}

const Split int32 = 256
const Match int32 = 257

func newState(c int32, out, out1 *State) *State {
	return &State{
		c:      c,
		out:    out,
		out1:   out1,
		lastid: -1,
	}
}

type Frag struct {
	start *State
	outs  []**State
}

func post2nfa(post string) *State {
	var stack [1000]Frag
	var sp int = -1

	push := func(f Frag) {
		sp++
		stack[sp] = f
	}

	pop := func() Frag {
		f := stack[sp]
		sp--
		return f
	}

	// outs are the dangling arrows in pointer list
	patch := func(outs []**State, dest *State) {
		for _, out := range outs {
			*out = dest
		}
	}

	list1 := func(s **State) []**State {
		list := make([]**State, 1)
		list[0] = s
		return list
	}

	var e1, e2 Frag
	for _, p := range post {
		switch p {
		case '.':
			e2, e1 = pop(), pop()
			patch(e1.outs, e2.start)
			push(Frag{e1.start, e2.outs})
		case '|':
			e2, e1 = pop(), pop()
			s := newState(Split, e1.start, e2.start)
			push(Frag{s, append(e1.outs, e2.outs...)})
		case '?':
			e1 = pop()
			s := newState(Split, e1.start, nil)
			push(Frag{s, append(e1.outs, &s.out1)})
		case '*':
			e1 = pop()
			s := newState(Split, e1.start, nil)
			patch(e1.outs, s)
			push(Frag{s, list1(&s.out1)})
		case '+':
			e1 = pop()
			s := newState(Split, e1.start, nil)
			patch(e1.outs, s)
			push(Frag{e1.start, list1(&s.out1)})
		default:
			s := newState(p, nil, nil)
			push(Frag{s, list1(&s.out)})
		}
	}
	e := pop()
	matchstate := newState(Match, nil, nil)
	patch(e.outs, matchstate)
	return e.start
}

func Compile(expr string) (*Regexp, error) {
	post, err := re2post(expr)
	if err != nil {
		return nil, err
	}
	regexp := &Regexp{post2nfa(post)}
	return regexp, nil
}

type SList struct {
	states []*State
	id     int
}

func (list *SList) addState(state *State) {
	if state.lastid == list.id {
		return
	}
	if state.c == Split {
		list.addState(state.out)
		list.addState(state.out1)
		return
	}
	list.states = append(list.states, state)
	state.lastid = list.id
}

func (re *Regexp) Match(str string) bool {
	clist := &SList{
		states: []*State{},
		id:     0,
	}
	clist.addState(re.start)
	for _, c := range str {
		nlist := &SList{
			states: []*State{},
			id:     clist.id + 1,
		}
		for _, state := range clist.states {
			if state.c == c {
				nlist.addState(state.out)
			}
		}
		clist = nlist
	}
	for _, state := range clist.states {
		if state.c == Match {
			return true
		}
	}
	return false
}
